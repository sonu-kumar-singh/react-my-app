import React from 'react';
import './index.css'
import logo from '/home/sonusingh/BootCamp/my-app/src/logo.svg'
// import icon from './icon.svg'
function NavBar () {
    return <div className="navBar">
           <div className="firstNav">
           <img src={logo} className="logo" alt="logo" />
            <a href="/react" className='reactName'>React</a>
           </div>
           <div className="secondNav">
            <a href="/docs" className='secondNavLink'>Docs</a>
            <a href="/tutorial" className='secondNavLink'>Tutorial</a>
            <a href="/blog" className='secondNavLink'>Blog</a>
            <a href="/community" className='secondNavLink'>Community</a>
            <input type='text' placeholder="Search..." className='search'></input>
           </div>
           <div className="thirdNav">
            <a href="/version" className='thirdNavLink'>v18.2.0</a>
            {/* <img src={icon} alt="icon"/> */}
            <a href="/languages" className='thirdNavLink'>Languages</a>
            <a href="github" className='thirdNavLink'>GitHub</a>
           </div>
    </div>


}

export default NavBar;