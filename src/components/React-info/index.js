import React from 'react';
import './index.css';
function ReactInfo () {
    return <div className='reactInfo'>
    <p className='reactLiteral'>React</p>
    <p className='paraIntro'>A JavaScript library for building user interfaces</p>
    <div className='startButton'>
        <button className="button">Get Started</button>
        <p>Take the Tutorial</p>
    </div>
    </div>
     
}
export default ReactInfo;