import React from 'react';
import './index.css';
function reactTheory() {
    return <div className="reactTheory">
        <div className="childReactTheory">
            <p className='stylePara'>Declarative</p>
            <div className='middleDiv'>React makes it painless to create interactive UIs. Design simple views for each state
                in your application, and React will efficiently update and render just the right
                components when your data changes.
            </div>
            <div>
                Declarative views make your code more predictable and easier to debug.
            </div>
        </div>

        <div className="childReactTheory">
            <p className='stylePara'>Component-Based</p>
            <div className='middleDiv'>
                Build encapsulated components that manage their own state, then compose them to 
                make complex UIs.
            </div>
            <div>
                Since component logic is written in JavaScript instead of templates, you can easily 
                pass rich data through your app and keep state out of the DOM.
            </div>
        </div>

        <div className="childReactTheory">
            <p className='stylePara'>Learn Once, Write Anywhere</p>
            <div className='middleDiv'>
                We don’t make assumptions about the rest of your technology stack, so you can develop
                new features in React without rewriting existing code.
            </div>
            <div>
                React can also render on the server using Node and power mobile apps using React Native.
            </div>
        </div>

    </div>
}

export default reactTheory;