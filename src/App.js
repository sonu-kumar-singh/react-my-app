// import logo from './logo.svg';
import React from 'react';
import './App.css';
import Head from './components/Notification';
import NavBar from './components/Nav-bar';
import ReactInfo from './components/React-info';
import ReactTheory from './components/React-theory';
function App() {
  return (
    <div className="App">
      <Head></Head>
      <NavBar></NavBar>
      <ReactInfo></ReactInfo>
      <ReactTheory></ReactTheory>  
    </div>
  );
}

export default App;
